Rails.application.routes.draw do
  
  get 'password_resets/new'
  get 'password_resets/edit'

  get 'page_not_found_error/not_found'
  get 'store/index'
  get 'users/new'
  get 'charts/sports_chart'
  get 'charts/pie_chart_form'
  get 'charts/hosts_chart'
  get 'charts/column_chart_form'
  
  root 'static_pages#home'

  get '/barchart', to: 'charts#medals_chart'
  get '/columncharts', to: 'charts#events_chart'
  get '/facebook', to: 'facebook#index'
  get '/store', to: 'store#index'
  get '/cart', to: 'carts#index'
  get '/about', to: 'static_pages#about'
  get '/home', to: 'static_pages#home'
  get  '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  delete '/cart.:id', to: 'carts#destroy'
  
  resources :results
  resources :hosts
  resources :sports
  resources :athletes
  resources :users
  resources :products
  resources :orders
  resources :line_items
  resources :carts
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  match '*a', :to => 'page_not_found_error#not_found', via: :get
end
