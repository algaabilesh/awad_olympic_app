require 'spec_helper'

describe 'products/show.html.erb' do
	it 'displays product details' do
		assign(:product, Product.create(title: 'Mascot', description: 'Mascot from Winter Olympics 2014', image_url: 'https://awad-olympic.com',
			price: 25.0))

		render

		rendered.should match('Mascot')
		rendered.should match('Mascot from Winter Olympics 2014')
		rendered.should match('https://awad-olympic.com')
		rendered.should match('25.0')
	end
end
