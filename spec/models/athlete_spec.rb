require 'rails_helper'

RSpec.describe Athlete, :type => :model do
	subject { described_class.new(athlete_surname: 'ABILESH', athlete_forename: 'Alga', gender: 'Male', country: 'KAZ')

	}

	it "is valid with valid attributes" do
		expect(subject).to be_valid
	end

	it "is not valid without a athlete_surname" do
		subject.athlete_surname = nil
		expect(subject).to_not be_valid
	end

	it "is not valid without a athlete_forename" do
		subject.athlete_forename = nil
		expect(subject).to_not be_valid
	end

	it 'is not valid without a gender' do
		subject.gender = nil
		expect(subject).to_not be_valid
	end

	it 'is not valid without a country' do
		subject.country = nil
		expect(subject).to_not be_valid
	end
end	