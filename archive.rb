# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180320105519) do

  create_table "athletes", force: :cascade do |t|
    t.string "athlete_surname"
    t.string "athlete_forename"
    t.string "country"
    t.string "gender"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hosts", force: :cascade do |t|
    t.integer "year"
    t.string "host_city"
    t.string "host_country"
    t.integer "number_of_events"
    t.integer "nations_participating"
    t.integer "number_of_athletes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "results", force: :cascade do |t|
    t.integer "host_id"
    t.integer "sport_id"
    t.string "athlete_forename"
    t.string "athlete_surname"
    t.string "country"
    t.string "gender"
    t.string "event"
    t.string "medal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sports", force: :cascade do |t|
    t.string "sport_name"
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end


    add_reference :results, :hosts, foreign_key: true


class Result < ApplicationRecord
  belongs_to :host
  belongs_to :sport
  belongs_to :athlete


  def self.most_medals
    Result.includes(:sport).group("athlete_forename").where("medal = 'Gold' OR medal = 'Silver' OR medal = 'Bronze'").order("count(medal) desc").limit(10).pluck("athlete_forename, count(medal) as medals_number")
  end

  def full_name 
    [athlete_surname, athlete_forename].join(' ')
  end     
end


<%= form_with url: athletes_path do |form|%>
  <div class="active-pink-3 active-pink-4 mb-4">
    <%= label_tag 'Search by athletes surname:' %>
    <%= form.text_field :athlete_surname %>
    <%= label_tag 'Filter by country:' %>
    <%= form.select 'country', options_for_select(Athlete.all.collect { |a| [ a.country] }.uniq),:prompt => 'Please select the country' %> 
    <%= label_tag 'Filter by gender:' %> 
    <%= label_tag 'Male' %>
    <%= form.radio_button 'gender','Male' %>
    <%= label_tag 'Female' %>
    <%= form.radio_button 'gender','Female'%>
    <%= form.submit 'Search', name: nil %>
  </div> 
<% end %>