class CreateHosts < ActiveRecord::Migration[5.1]
  def change
    create_table :hosts do |t|
      t.integer :year
      t.string :host_city
      t.string :host_country
      t.integer :number_of_events
      t.integer :nations_participating
      t.integer :number_of_athletes

      t.timestamps
    end
  end
end
