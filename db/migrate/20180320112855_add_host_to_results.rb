class AddHostToResults < ActiveRecord::Migration[5.1]
  def change
    add_reference :results, :host, foreign_key: true
  end
end
