class DeleteUsersFromCarts < ActiveRecord::Migration[5.1]
  def change
  	remove_foreign_key :carts, :users
  end
end
