class AddSportToResult < ActiveRecord::Migration[5.1]
  def change
    add_reference :results, :sport, foreign_key: true
  end
end
