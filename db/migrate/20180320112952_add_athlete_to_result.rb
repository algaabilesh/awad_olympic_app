class AddAthleteToResult < ActiveRecord::Migration[5.1]
  def change
    add_reference :results, :athlete, foreign_key: true
  end
end
