class CreateAthletes < ActiveRecord::Migration[5.1]
  def change
    create_table :athletes do |t|
      t.string :athlete_surname
      t.string :athlete_forename
      t.string :country
      t.string :gender

      t.timestamps
    end
  end
end
