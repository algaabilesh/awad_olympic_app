class DeleteUserIndexFromCarts < ActiveRecord::Migration[5.1]
  def change
  	remove_index :carts, column: :user_id
  end
end
