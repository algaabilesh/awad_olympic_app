# Winter Olympic Games Website README

The Winter Olympic Games Website uses open data that can be available from https://www.kaggle.com/the-guardian/olympic-games and 
in this app you can view the results of different sportsmen who won medals in last 10 Winter Olympic Games 1980-2014.
So in this website you can view results in form of pie charts, bar charts and column charts. The application also uses live 
facebook page plugin data, displaying the posts from Olympics official Facebook page in real time. Also app provides the opportunity
to authorize users using email validation, and users must been authorized in order to view pages of website. Also users can search 
different athletes across range of filters that available. Users can also order some Olympic Games related items and view some products. 

How to run:

- Locally:
    In the Terminal, navigate to the folder in which the application is installed and type the following commands:
    
    -bundle install to install all required gems
	-rails db:migrate to run the database migrations
	-rake awad:seed_awad to parse the data from the CSV files in the lib/assets folder
	-rails db:seed to seed the database with sample users
	-rails server to start the Puma server
	
   Open a web-browser and navigate to localhost:3000 to open the application

- Deploying to Heroku
 	
 	First install gem 'pg' into the production group in the Gemfile
 	Change the location of 'sqlite3' gem into the group 'development,test', then bundle install to install all gems
 	Push all changes to the Bitbucket in order to push commits to the Heroku
 	
 	In the Terminal, navigate to the folder in which the application is installed and type the following commands:

 	- heroku login - to log in with your Heroku account credentials
 	- heroku create <name>- creates a new empty application on Heroku, along with an associated empty Git repository with given name
 	- git push heroku master - push the code from your local repository’s master branch to your heroku remote
 	- heroku run rake db:migrate - to run the database migrations
 	- heroku run rake awad:seed_awad - to parse the data from the CSV files
 	- heroku run rake db:seed - to seed the database with sample users
 	- heroku open - to launch the application on the Heroku server

•	The application was deployed to Heroku in production environment, using PostgreSQL
•	Access the application from any browser via the following url: https://awad-olympics.herokuapp.com/