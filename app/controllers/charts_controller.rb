class ChartsController < ApplicationController
  before_action :logged_in_user, only: [:medals_chart, :events_chart, :sports_chart]
  def medals_chart
  end

  def events_chart
  end

  def sports_chart
  	@sport = params[:sport]
  	@sport_name = Sport.find(@sport[:id]).sport_name
  end	

  def pie_chart_form
  	@sport = params[:sport]
  end

  def hosts_chart
  	@host = params[:host]
  	@host_name = Host.find(@host[:id]).host_city + ", " + Host.find(@host[:id]).host_country + ", " + Host.find(@host[:id]).year.to_s
  end

  def column_chart_form
  end	
end
