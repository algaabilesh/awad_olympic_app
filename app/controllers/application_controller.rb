class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  include SessionsHelper

   # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in to view this page!"
        redirect_to login_url
      end
    end

    # Confirms an admin user.
    def admin_user
      if current_user.nil?
        flash[:danger] = "Please log in!"
        redirect_to login_url
      else  
        unless current_user.admin?
          flash[:danger] = "You don't have access to view this page!"
          redirect_to root_url
        end 
      end  
    end
end
