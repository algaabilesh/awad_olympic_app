class StoreController < ApplicationController
  before_action :logged_in_user, only: [:index]
  include CurrentCart
  before_action :set_cart

  def index
  	@products = Product.order(:title)
  end
end
