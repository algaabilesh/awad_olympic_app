json.extract! result, :id, :host_id, :sport_id, :athlete_forename, :athlete_surname, :country, :gender, :event, :medal, :created_at, :updated_at
json.url result_url(result, format: :json)
