json.extract! host, :id, :year, :host_city, :host_country, :number_of_events, :nations_participating, :number_of_athletes, :created_at, :updated_at
json.url host_url(host, format: :json)
