json.extract! athlete, :id, :athlete_surname, :athlete_forename, :country, :gender, :created_at, :updated_at
json.url athlete_url(athlete, format: :json)
