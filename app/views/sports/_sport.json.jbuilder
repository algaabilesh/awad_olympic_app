json.extract! sport, :id, :sport_name, :category, :created_at, :updated_at
json.url sport_url(sport, format: :json)
