class Order < ApplicationRecord
	has_many :line_items, dependent: :destroy
	validates :name, :address, :email, presence: true
	
	enum pay_type: {
		"Credit card" => 0,
		"PayPal" => 1,
		"Gift Card" => 2,
		"Other payemnt method" => 3
	}

	def add_line_items_from_cart(cart)
 		cart.line_items.each do |item|
	 		item.cart_id = nil
	 		line_items << item
		end
 	end
end
