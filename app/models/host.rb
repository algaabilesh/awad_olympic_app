class Host < ApplicationRecord
	has_many :results

	def host_with_year 
		[host_city, host_country, year].join(',')
	end	
end
