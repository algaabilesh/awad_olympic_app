class Result < ApplicationRecord
  belongs_to :host
  belongs_to :sport
  belongs_to :athlete


  def full_name 
    [athlete_surname, athlete_forename].join(' ')
  end     

  def self.pie_chart
  	Result.includes(:athlete).includes(:sport).group('athlete_id', 'athlete_surname').where("medal = 'Gold' OR medal = 'Silver' OR medal = 'Bronze'").order("count(medal) desc").limit(5).pluck("athlete_surname, count(medal) as medals_number")
  end	

  def self.column_chart
  	Result.includes(:athlete).includes(:host).group('athlete_id', 'athlete_surname').where("medal = 'Gold' OR medal = 'Silver' OR medal = 'Bronze'").order("count(medal) desc").limit(5).pluck("athlete_surname, count(medal) as medals_number")
  end
end