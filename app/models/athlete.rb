class Athlete < ApplicationRecord
	validates_presence_of :athlete_surname, :athlete_forename, :gender, :country
	has_many :results
	scope :athlete_surname, -> athlete_surname { where('athlete_surname LIKE ?', "%#{athlete_surname.upcase.gsub(/\s+/, "")}%")}
	scope :country, -> country { where(:country => country)}
	scope :gender, -> gender { where(:gender => gender)}

  def self.most_medals
    Athlete.includes(:results).group(:id, :athlete_surname).where("results.medal = 'Gold' OR results.medal = 'Silver' OR results.medal = 'Bronze'").order("count(results.medal) desc").limit(10).pluck("athlete_surname, count(results.medal) as medals_number")
  end
	
end
