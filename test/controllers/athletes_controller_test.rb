require 'test_helper'

class AthletesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @athlete = athletes(:one)
  end

  test "should get new" do
    get new_athlete_url
    assert_response :success
  end

  test "should create athlete" do
    assert_difference('Athlete.count') do
      post athletes_url, params: { athlete: { athlete_forename: @athlete.athlete_forename, athlete_surname: @athlete.athlete_surname, country: @athlete.country, gender: @athlete.gender } }
    end

    assert_redirected_to athlete_url(Athlete.last)
  end

  test "should show athlete" do
    get athlete_url(@athlete)
    assert_response :success
  end


  test "should destroy athlete" do
    assert_difference('Athlete.count', 0) do
      delete athlete_url(@athlete)
    end

    assert_redirected_to login_url
  end
end
