require 'test_helper'

class LineItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @line_item = line_items(:one)
  end

  test "should get new" do
    get new_line_item_url
    assert_response :success
  end


  test "should show line_item" do
    get line_item_url(@line_item)
    assert_response :success
  end

  test "should destroy line_item" do
    assert_difference('LineItem.count', 0) do
      delete line_item_url(@line_item)
    end

    assert_redirected_to login_url
  end
end
