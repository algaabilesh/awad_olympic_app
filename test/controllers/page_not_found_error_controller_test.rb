require 'test_helper'

class PageNotFoundErrorControllerTest < ActionDispatch::IntegrationTest
  test "should get not_found" do
    get page_not_found_error_not_found_url
    assert_response :success
  end

end
