require 'csv'
namespace :awad do
  desc "pull olymic games data into database"
  task seed_awad: :environment do

    #drop the old table data before importing the new stuff
    

    CSV.foreach("lib/assets/hosts.csv", :headers =>true, encoding: 'iso-8859-1:utf-8') do |row |
      puts row.inspect #just so that we know the file's being read

      #create new model instances with the data
      Host.create!(
      year: row[0],
      host_city: row[1],
      host_country: row[2],
      number_of_events: row[3],
      nations_participating: row[4],
      number_of_athletes: row[5]
    )
    end

    CSV.foreach("lib/assets/sports.csv", :headers =>true, encoding: 'iso-8859-1:utf-8') do |row |
      puts row.inspect #just so that we know the file's being read

      #create new model instances with the data
      Sport.create!(
      sport_name: row[0],
      category: row[1]
    )
    end

    CSV.foreach("lib/assets/athletes.csv", :headers =>true, encoding: 'iso-8859-1:utf-8') do |row |
      puts row.inspect #just so that we know the file's being read

      #create new model instances with the data
      Athlete.create!(
      athlete_surname: row[0],
      athlete_forename: row[1],
      country: row[2],
      gender: row[3]
    )
    end
    

    CSV.foreach("lib/assets/results.csv", :headers =>true, encoding: 'iso-8859-1:utf-8') do |row |
      puts row.inspect #just so that we know the file's being read

      athlete = Athlete.where("athlete_surname = :athlete_surname and athlete_forename = :athlete_forename", { athlete_surname: row[2], athlete_forename: row[3]}).first
      athlete_id = athlete ? athlete.id : nil

      #create new model instances with the data
      Result.create!(
      host_id: row[0],
      sport_id: row[1],
      athlete_id: athlete_id,
      event: row[6],
      medal: row[7]
    )
    end

   
  end

end